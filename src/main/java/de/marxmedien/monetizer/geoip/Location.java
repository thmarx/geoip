/**
 * GeoIP
 * Copyright (C) 2016  ThorstenMarx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.marxmedien.monetizer.geoip;

import java.io.Serializable;

public class Location implements Serializable {

	public static final Location UNKNOWN = new Location("", "");
	private static final long serialVersionUID = 859549654205407080L;

	private String id = "";
	
	private String country = "";
	private String city = "";
	private String postalcode = "";

	private String latitude = "";
	private String longitude = "";
	
	private String timezone;

	public Location () {
		
	}
	
	public Location(String country, String city) {
		this.country = country;
		this.city = city;
	}

	public Location(String country, String city, String latitude, String lonitude) {
		this(country, city);
		this.latitude = latitude;
		this.longitude = lonitude;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getCountry() {
		return country;
	}

	

	public String getCity() {
		return city;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	
	
	

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Location other = (Location) obj;

		return this.city.equals(other.city) && this.country.equals(other.country)
				&& this.timezone.equals(other.timezone) && this.getPostalcode().equals(other.getPostalcode()) && this.latitude.equals(other.latitude)
				&& this.longitude.equals(other.longitude);
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 19 * hash + (this.city != null ? this.city.hashCode() : 0);
		hash = 19 * hash + (this.country != null ? this.country.hashCode() : 0);
		hash = 19 * hash + (this.timezone != null ? this.timezone.hashCode() : 0);
		hash = 19 * hash + (this.getPostalcode() != null ? this.getPostalcode().hashCode() : 0);
		hash = 19 * hash + (this.latitude != null ? this.latitude.hashCode() : 0);
		hash = 19 * hash + (this.longitude != null ? this.longitude.hashCode() : 0);
		return hash;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the postalcode
	 */
	public String getPostalcode() {
		return postalcode;
	}

	/**
	 * @param postalcode the postalcode to set
	 */
	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
}
