/**
 * GeoIP
 * Copyright (C) 2016  ThorstenMarx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.marxmedien.monetizer.geoip;

import java.sql.SQLException;

public interface IPLocationDB {
	
	public enum Mode {
		Read, Import
	}

	public void open(String db, Mode mode);

	public void close();

	public void importCountry(String path);

	public Location searchIp(String ip);

}