/**
 * GeoIP
 * Copyright (C) 2016  ThorstenMarx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.marxmedien.monetizer.geoip.impl.sample.lucene;


import de.marxmedien.monetizer.geoip.IPLocationDB;
import de.marxmedien.monetizer.geoip.impl.MaxmindIpLocationLucene;
import de.marxmedien.monetizer.geoip.impl.MaxmindIpLocationMapDB;




public class LuceneImport {
	public static void main(String[] args) throws Exception {

		IPLocationDB uplocDB = new MaxmindIpLocationLucene();
		uplocDB.open("/workspaces/testdata/lucene/iplocation", IPLocationDB.Mode.Import);

		long before = System.currentTimeMillis();
		System.out.println("start import maxmind");
		uplocDB.importCountry("/workspaces/testdata/GeoLite2-City-CSV_20161101");
		long after = System.currentTimeMillis();

		System.out.println("end import after: " + (after - before) + "ms");

		uplocDB.close();
	}
}
