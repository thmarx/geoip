/**
 * GeoIP
 * Copyright (C) 2016  ThorstenMarx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.marxmedien.monetizer.geoip.impl;

import com.google.common.base.Strings;
import com.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

import de.marxmedien.monetizer.geoip.IPLocationDB;
import de.marxmedien.monetizer.geoip.Location;
import de.marxmedien.monetizer.geoip.helper.CIDRUtils;
import de.marxmedien.monetizer.geoip.helper.ValidateIP;
import java.io.File;
import java.nio.file.Paths;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/**
 * Reader zum einlesen
 *
 * "ip_start";"country_code";"country_name";"region_code";"region_name";"city";
 * "zipcode";"latitude";"longitude";"metrocode"
 *
 * @author tmarx
 *
 */
public class MaxmindIpLocationLucene implements IPLocationDB {

	private final static Logger LOGGER = Logger.getLogger(MaxmindIpLocationLucene.class.getName());

	ConcurrentHashMap<Long, Location> ipLocations;
	ConcurrentHashMap<String, Location> locations;

	IndexWriter indexWriter;
	IndexSearcher indexSearcher;
	IndexReader indexReader;

	public MaxmindIpLocationLucene() {
	}

	/* (non-Javadoc)
	 * @see de.marx.services.geo.IPLocationDB#open()
	 */
	@Override
	public void open(final String db, final Mode mode) {

		File path = new File(db);
		if (!path.exists()) {
			path.mkdirs();
		}

		ipLocations = new ConcurrentHashMap<>();
		locations = new ConcurrentHashMap<>();

		if (Mode.Import.equals(mode)) {

			try {
				Directory dir = FSDirectory.open(Paths.get(db));
				Analyzer analyzer = new StandardAnalyzer();
				IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
				iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
				iwc.setRAMBufferSizeMB(256.0);
				indexWriter = new IndexWriter(dir, iwc);
			} catch (IOException ex) {
				Logger.getLogger(MaxmindIpLocationLucene.class.getName()).log(Level.SEVERE, null, ex);
				throw new IllegalArgumentException(ex);
			}
		} else {
			try {
				indexReader = DirectoryReader.open(FSDirectory.open(Paths.get(db)));
				indexSearcher = new IndexSearcher(indexReader);
			} catch (IOException ex) {
				Logger.getLogger(MaxmindIpLocationLucene.class.getName()).log(Level.SEVERE, null, ex);
				throw new IllegalArgumentException(ex);
			}
		}
	}

	/* (non-Javadoc)
	 * @see de.marx.services.geo.IPLocationDB#close()
	 */
	@Override
	public void close() {
		if (indexWriter != null) {
			try {
				indexWriter.close();
			} catch (IOException ex) {
				Logger.getLogger(MaxmindIpLocationLucene.class.getName()).log(Level.SEVERE, null, ex);
				throw new IllegalStateException(ex);
			}
		}
		if (indexReader != null){
			try {
				indexReader.close();
			} catch (IOException ex) {
				Logger.getLogger(MaxmindIpLocationLucene.class.getName()).log(Level.SEVERE, null, ex);
				throw new IllegalStateException(ex);
			}
		}
	}

	/* (non-Javadoc)
	 * @see de.marx.services.geo.IPLocationDB#importCountry(java.lang.String)
	 */
	@Override
	public void importCountry(String path) {
		try {
			if (!path.endsWith("/")) {
				path += "/";
			}

			// import locations
			System.out.println("import locations");
			getLocations(path);

			BufferedReader br = new BufferedReader(new FileReader(path + "GeoLite2-City-Blocks-IPv4.csv"));
			CSVReader reader = new CSVReader(br, ',', '\"', 2);

			int count = 0;
			String[] values;
			System.out.println("create location searchtree");
			while ((values = reader.readNext()) != null) {
				String cidr = values[0];
				String locid = values[1];
				if (Strings.isNullOrEmpty(locid)) {
					locid = values[2];
				}
				if (Strings.isNullOrEmpty(locid)) {
					locid = values[3];
				}

				CIDRUtils cidrUtil = new CIDRUtils(cidr);

				Location location = locations.get(locid);
				if (location == null) {
					LOGGER.log(Level.INFO, "location not found " + cidr);
					continue;
				}

				String startIP = cidrUtil.getStartIp();
				long ipFrom = ValidateIP.ip2long(startIP);
				String endIP = cidrUtil.getEndIp();
				long ipTo = ValidateIP.ip2long(endIP);

				location.setPostalcode(values[6]);
				location.setLatitude(values[7]);
				location.setLongitude(values[8]);

				Document document = new Document();
				document.add(new LongPoint("ip_from", ipFrom));
				document.add(new LongPoint("ip_to", ipTo));
				document.add(new StringField("id", location.getId(), Field.Store.YES));
				document.add(new StringField("country", location.getCountry(), Field.Store.YES));
				document.add(new StringField("city", location.getCity(), Field.Store.YES));
				document.add(new StringField("postalcode", location.getPostalcode(), Field.Store.YES));
				document.add(new StringField("timezone", location.getTimezone(), Field.Store.YES));
				document.add(new StringField("latitude", location.getLatitude(), Field.Store.YES));
				document.add(new StringField("longitude", location.getLongitude(), Field.Store.YES));

				indexWriter.addDocument(document);

				count++;

				if (count % 10000 == 0) {
					LOGGER.log(Level.INFO, "{0} Locations in searchtree", count);
				}
			}
			locations.clear();

			LOGGER.log(Level.INFO, "{0} entries imported", count);

		} catch (IOException | NumberFormatException e) {
			LOGGER.log(Level.SEVERE, "", e);
		} finally {
			this.locations.clear();
			this.ipLocations.clear();
		}
	}

	/* (non-Javadoc)
	 * @see de.marx.services.geo.IPLocationDB#searchIp(java.lang.String)
	 */
	@Override
	public Location searchIp(String ip) {
		try {

			long inetAton = ValidateIP.ip2long(ip);
			
			BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
			Query fromQuery = LongPoint.newRangeQuery("ip_from", Long.MIN_VALUE, inetAton);
			Query toQuery = LongPoint.newRangeQuery("ip_to", inetAton, Long.MAX_VALUE);

			queryBuilder.add(fromQuery, BooleanClause.Occur.MUST);
			queryBuilder.add(toQuery, BooleanClause.Occur.MUST);
			
			TopDocs topDocs = indexSearcher.search(queryBuilder.build(), 1);
			
			if (topDocs.totalHits != 0){
				Document document = indexReader.document(topDocs.scoreDocs[0].doc);
				
				Location location = new Location();
				location.setId(document.get("id"));
				location.setCity(document.get("city"));
				location.setPostalcode(document.get("postalcode"));
				location.setCountry(document.get("country"));
				location.setTimezone(document.get("timezone"));
				location.setLatitude(document.get("latitude"));
				location.setLongitude(document.get("longitude"));
				
				return location;
			}
			
			return Location.UNKNOWN;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "", e);
		} finally {

		}

		return null;
	}

	private void getLocations(String path) throws IOException {
		if (!path.endsWith("/")) {
			path += "/";
		}
		String filename = path + "GeoLite2-City-Locations-de.csv";

		BufferedReader br = new BufferedReader(new FileReader(filename));
		CSVReader reader = new CSVReader(br, ',', '\"', 2);
		String[] values;
		int count = 0;
		while ((values = reader.readNext()) != null) {

			Location location = new Location();
			location.setId(values[0]);
			location.setCountry(values[4]);
//			location.setRegionName(values[2]);
			location.setCity(values[10]);
			location.setTimezone(values[12]);
//			location.setPostalcode(values[4]);
//			location.setLatitude(values[5]);
//			location.setLongitude(values[6]);

			this.locations.put(values[0], location);

			count++;
			if (count % 10000 == 0) {
//				this.db.commit();
				System.out.println(count + " locations importiert");
			}
		}
	}

	private String mtrim(String text) {
		if (text.startsWith("\"")) {
			text = text.substring(1);
		}
		if (text.endsWith("\"")) {
			text = text.substring(0, text.length() - 1);
		}
		return text;
	}
}
