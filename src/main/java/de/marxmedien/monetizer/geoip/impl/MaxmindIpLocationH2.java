/**
 * GeoIP
 * Copyright (C) 2016  ThorstenMarx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.marxmedien.monetizer.geoip.impl;



import com.google.common.base.Strings;
import com.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import de.marxmedien.monetizer.geoip.IPLocationDB;
import de.marxmedien.monetizer.geoip.Location;
import de.marxmedien.monetizer.geoip.helper.CIDRUtils;
import de.marxmedien.monetizer.geoip.helper.ValidateIP;
import java.io.File;
import java.util.HashMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.h2.mvstore.MVMap;
import org.h2.mvstore.MVStore;
import org.h2.mvstore.rtree.MVRTreeMap;


/**
 * Reader zum einlesen
 * 
 * "ip_start";"country_code";"country_name";"region_code";"region_name";"city";
 * "zipcode";"latitude";"longitude";"metrocode"
 * 
 * @author tmarx
 * 
 */
public class MaxmindIpLocationH2 implements IPLocationDB {
	
    private final static Logger LOGGER = Logger.getLogger(MaxmindIpLocationH2.class.getName());
    
	MVMap<Long, String> treeMap = null;
	MVMap<String, Location> locations;
	MVStore store;
	
	public MaxmindIpLocationH2() {
	}
	
	/* (non-Javadoc)
	 * @see de.marx.services.geo.IPLocationDB#open()
	 */
	@Override
	public void open(String db, final Mode mode){
		this.store = new MVStore.Builder()
				.fileName(db)
				.open();
		
		this.treeMap = this.store.openMap("ip2locid");
		this.locations = this.store.openMap("locid2location");
	}

	/* (non-Javadoc)
	 * @see de.marx.services.geo.IPLocationDB#close()
	 */
	@Override
	public void close() {
		store.close();
//		poolMgr.dispose();
	}

	/* (non-Javadoc)
	 * @see de.marx.services.geo.IPLocationDB#importCountry(java.lang.String)
	 */
	@Override
	public void importCountry(String path) {
		try {
			if (!path.endsWith("/")) {
				path += "/";
			}
			
			// import locations
			System.out.println("import locations");
			getLocations(path);
			
			BufferedReader br = new BufferedReader(new FileReader(path + "GeoLite2-City-Blocks-IPv4.csv"));
			CSVReader reader = new CSVReader(br, ',', '\"', 2);
			
			int count = 0;
			String [] values;
			System.out.println("create location searchtree");
			while ((values = reader.readNext()) != null) {
				String cidr = values[0];
				String locid = values[1];
				if (Strings.isNullOrEmpty(locid)) {
					locid = values[2];
				}
				if (Strings.isNullOrEmpty(locid)) {
					locid = values[3];
				}

				CIDRUtils cidrUtil = new CIDRUtils(cidr);
				
				Location location = locations.get(locid);
				if (location == null) {
					LOGGER.log(Level.INFO, "location not found " + cidr);
					continue;
				}
				
				String startIP = cidrUtil.getStartIp();
				long ipFrom =  ValidateIP.ip2long(startIP);
				
				location.setPostalcode(values[6]);
				location.setLatitude(values[7]);
				location.setLongitude(values[8]);
				
				this.treeMap.put(ipFrom, locid);
				
				count++;
				
				if (count % 10000 == 0) {
//					this.db.commit();
					LOGGER.info(count + " Locations in searchtree");
				}
			}
//			locations.clear();
			LOGGER.info(count + " entries imported");
			
		} catch (IOException | NumberFormatException e) {
			LOGGER.log(Level.SEVERE, "", e);
		} finally {
//			this.db.close();
		}
	}
	
	/* (non-Javadoc)
	 * @see de.marx.services.geo.IPLocationDB#searchIp(java.lang.String)
	 */
	@Override
	public Location searchIp(String ip) {
		try {
			
	        
	        long inetAton = ValidateIP.ip2long(ip);
            
			Long key = this.treeMap.lowerKey(inetAton);
			
			if (key != null) {
				 return this.locations.get(this.treeMap.get(key));
			}
			
	        
	        return Location.UNKNOWN;
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "", e);
		} finally {
			
		}
		
		return null;
	}
	
	private void getLocations (String path) throws IOException {
		if (!path.endsWith("/")) {
			path += "/";
		}
		String filename = path + "GeoLite2-City-Locations-de.csv";
		
		BufferedReader br = new BufferedReader(new FileReader(filename));
		CSVReader reader = new CSVReader(br, ',', '\"', 2);
		String [] values;
		int count = 0;
		while ((values = reader.readNext()) != null) {
			
			
			Location location = new Location();
			location.setCountry(values[4]);
//			location.setRegionName(values[2]);
			location.setCity(values[10]);
			location.setTimezone(values[12]);
//			location.setPostalcode(values[4]);
//			location.setLatitude(values[5]);
//			location.setLongitude(values[6]);
			
			this.locations.put(values[0], location);
			
			count++;
			if (count % 10000 == 0) {
//				this.db.commit();
				System.out.println(count + " locations importiert");
			}
		}
		
//		this.db.compact();
	}
	
	private String mtrim (String text) {
		if (text.startsWith("\"")) {
			text = text.substring(1);
		}
		if (text.endsWith("\"")) {
			text = text.substring(0, text.length()-1);
		}
		return text;
	}
}
