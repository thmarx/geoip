/**
 * GeoIP
 * Copyright (C) 2016  ThorstenMarx (kontakt@thorstenmarx.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.marxmedien.monetizer.geoip.impl.sample.mvstore;


import de.marxmedien.monetizer.geoip.IPLocationDB;
import de.marxmedien.monetizer.geoip.Location;
import de.marxmedien.monetizer.geoip.impl.MaxmindIpLocationH2;




public class MVStoreSample {
	
public static void main(String[] args) throws Exception {
		
		IPLocationDB readerhsql = new MaxmindIpLocationH2();
		readerhsql.open("/workspaces/testdata/mvstore/db", IPLocationDB.Mode.Read);
		
		testLocation(readerhsql, "66.211.160.129");
		System.out.println("--------------------");
		testLocation(readerhsql, "213.83.37.145");
		System.out.println("--------------------");
		testLocation(readerhsql, "88.153.215.174");
		System.out.println("--------------------");
		testLocation(readerhsql, "88.153.198.42");
		System.out.println("--------------------");
		testLocation(readerhsql, "85.22.92.225");
		System.out.println("--------------------");
		
		readerhsql.close();
	}

	private static void testLocation(IPLocationDB readerhsql, String ip) {
		long before = System.currentTimeMillis();
		Location loc = readerhsql.searchIp(ip);
		System.out.println("ID: " + loc.getId());
		System.out.println("1: country / city : " + loc.getCountry() + " - " + loc.getCity());
		System.out.println("2: postalcode / timezone : " + loc.getPostalcode() + " - " + loc.getTimezone());
		System.out.println("3: lat / lng : " + loc.getLatitude() + " - " + loc.getLongitude());
		long after = System.currentTimeMillis();
		System.out.println((after - before) + "ms");
	}
}
