/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.marxmedien.monetizer.geoip.impl.sample.maxmind;

import com.fasterxml.jackson.databind.JsonNode;
import com.maxmind.db.Reader;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.text.NumberFormat;

/**
 *
 * @author marx
 */
public class MaxmindTest {

	public static void main(String... args) throws IOException {
		memory();
		File database = new File("/workspaces/testdata/GeoLite2-City.mmdb");
		Reader reader = new Reader(database);
		memory();
		InetAddress address = InetAddress.getByName("85.22.92.225");

		JsonNode response = reader.get(address);

		System.out.println(response);

		reader.close();
	}

	private static void memory() {
		Runtime runtime = Runtime.getRuntime();

		NumberFormat format = NumberFormat.getInstance();

		StringBuilder sb = new StringBuilder();
		long maxMemory = runtime.maxMemory();
		long allocatedMemory = runtime.totalMemory();
		long freeMemory = runtime.freeMemory();

		sb.append("free memory: ").append(format.format(freeMemory / 1024)).append("\r\n");
		sb.append("allocated memory: ").append(format.format(allocatedMemory / 1024)).append("\r\n");
		sb.append("max memory: ").append(format.format(maxMemory / 1024)).append("\r\n");
		sb.append("total free memory: ").append(format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024)).append("\r\n");
		
		System.out.println(sb.toString());
	}
}
